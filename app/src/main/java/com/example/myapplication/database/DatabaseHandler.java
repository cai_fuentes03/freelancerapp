package com.example.myapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import com.example.myapplication.data.TVData;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Android 18 on 5/11/2015.
 */
public class DatabaseHandler extends SQLiteOpenHelper{

    private static int DATABASE_VERSION = 1;
    private static String DATABASE_NAME = "database.db";

    private static final String TABLE_LIST = "tv_programs";

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_START = "start_time";
    private static final String KEY_END = "end_time";
    private static final String KEY_CHANNEL = "channel";
    private static final String KEY_RATING = "rating";

    public DatabaseHandler(Context context){
        super(context, Environment.getExternalStorageDirectory()
                + File.separator + "freelancer"
                + File.separator + DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_LIST + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_START + " TEXT, " + KEY_END + " TEXT, " +  KEY_CHANNEL + " TEXT, " + KEY_RATING + " TEXT " + ")";
        sqLiteDatabase.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_LIST);
        onCreate(sqLiteDatabase);
    }

    public void addTVProgram(TVData data) {
        ArrayList<TVData> list = new ArrayList<TVData>();

        SQLiteDatabase db = this.getWritableDatabase();
        if(list.size()>0){
            for(int x=0; x<list.size(); x++){
                if(list.get(x).getName().trim().equals(data.getName().trim())){

                }else{
                    ContentValues values = new ContentValues();
                    values.put(KEY_NAME, data.getName());
                    values.put(KEY_START, data.getStart_time());
                    values.put(KEY_END, data.getEnd_time());
                    values.put(KEY_CHANNEL, data.getChannel());
                    values.put(KEY_RATING, data.getRating());

                    // Inserting Row
                    db.insert(TABLE_LIST, null, values);
                }
            }
        }else{
            ContentValues values = new ContentValues();
            values.put(KEY_NAME, data.getName());
            values.put(KEY_START, data.getStart_time());
            values.put(KEY_END, data.getEnd_time());
            values.put(KEY_CHANNEL, data.getChannel());
            values.put(KEY_RATING, data.getRating());

            // Inserting Row
            db.insert(TABLE_LIST, null, values);
        }

//        db.close(); // Closing database connection
    }

    public List<Integer> getAllIDs(){
        List<Integer> ids = new ArrayList<Integer>();

        String selectQuery = "SELECT  * FROM " + TABLE_LIST;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                ids.add(Integer.parseInt(cursor.getString(0)));
            } while (cursor.moveToNext());
        }

        return ids;
    }

    public List<TVData> getAllPrograms() {
        List<TVData> list = new ArrayList<TVData>();

        String selectQuery = "SELECT  * FROM " + TABLE_LIST;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                TVData d = new TVData();
                d.setId(Integer.parseInt(cursor.getString(0)));
                d.setName(cursor.getString(1));
                d.setStart_time(cursor.getString(2));
                d.setEnd_time(cursor.getString(3));
                d.setChannel(cursor.getString(4));
                d.setRating(cursor.getString(5));

                list.add(d);
            } while (cursor.moveToNext());
        }

        return list;
    }
}
