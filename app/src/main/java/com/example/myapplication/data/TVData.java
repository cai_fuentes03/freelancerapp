package com.example.myapplication.data;

/**
 * Created by Android 18 on 5/11/2015.
 */
public class TVData {

    public int id;
    public String name;
    public String start_time;
    public String end_time;
    public String channel;
    public String rating;

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return this.id;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setStart_time(String start_time){
        this.start_time = start_time;
    }

    public String getStart_time(){
        return this.start_time;
    }

    public void setEnd_time(String end_time){
        this.end_time = end_time;
    }

    public String getEnd_time(){
        return this.end_time;
    }

    public void setChannel(String channel){
        this.channel = channel;
    }

    public String getChannel(){
        return this.channel;
    }

    public void setRating(String rating){
        this.rating = rating;
    }

    public String getRating(){
        return this.rating;
    }

}
