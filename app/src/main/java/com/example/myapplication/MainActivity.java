package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.myapplication.apiservice.OnServiceListener;
import com.example.myapplication.apiservice.Service;
import com.example.myapplication.apiservice.ServiceAction;
import com.example.myapplication.apiservice.ServiceResponse;
import com.example.myapplication.data.TVData;
import com.example.myapplication.data.TVList;
import com.example.myapplication.database.DatabaseHandler;

import java.util.ArrayList;


public class MainActivity extends Activity implements OnServiceListener{

    private ArrayList<TVData> list;
    private ListView listView;
    private int start = 0;

    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseHandler = new DatabaseHandler(MainActivity.this);
        list = new ArrayList<TVData>();
        listView = (ListView) findViewById(R.id.listview);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0) {
                    // check if we reached the top or bottom of the list
                    View v = listView.getChildAt(0);
                    int offset = (v == null) ? 0 : v.getTop();
                    if (offset == 0) {
                        // reached the top:
                        return;
                    }
                } else if (totalItemCount - visibleItemCount == firstVisibleItem) {
                    View v = listView.getChildAt(totalItemCount - 1);
                    int offset = (v == null) ? 0 : v.getTop();
                    if (offset == 0) {
//                        Toast.makeText(MainActivity.this, "Reached the Bottom", Toast.LENGTH_SHORT).show();

                        start += 10;
                        callAPI(String.valueOf(start));
                        return;
                    }
                }
            }
        });


        if(checkConnection(this)){
            callAPI(String.valueOf(start));
        }else{
            Toast.makeText(MainActivity.this, "No Internet Connection.\nRetrieving Stored Offline List", Toast.LENGTH_LONG).show();
            try{
                ArrayList<TVData> l = (ArrayList<TVData>) databaseHandler.getAllPrograms();
//                Toast.makeText(MainActivity.this, "offline size = " + l.size(), Toast.LENGTH_LONG).show();
                DataAdapter adapter = new DataAdapter(MainActivity.this, l);
                listView.setAdapter(adapter);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public void callAPI(String start){
        try{
            Service service = new Service(this);
            service.call_api(start);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onCompleted(Service service, ServiceResponse result) {
        try{
            if(result==null || result.getData()==null) {
                Toast.makeText(this, "All Programs are Displayed", Toast.LENGTH_SHORT).show();
            }
            else if(result.getAction()== ServiceAction.ActionCallAPI){
                TVList data =  (TVList) result.getData();
                if(data.status==1){

                    if(data.data_list.size()>0){

                        list.addAll(data.data_list);

                        DataAdapter adapter = new DataAdapter(MainActivity.this, list);
                        listView.setAdapter(adapter);

                        ArrayList<TVData> l = (ArrayList<TVData>) databaseHandler.getAllPrograms();
                        ArrayList<Integer> ids = (ArrayList<Integer>) databaseHandler.getAllIDs();

                        // offline db have data
                        if(l.size()>0){
                            for(int x=0; x<list.size();x++){

                                if(ids.contains(list.get(x).getId())){
                                    // data alr in offline db
                                }else{
                                    databaseHandler.addTVProgram(list.get(x));
                                }
                            }

                        }else{ // offline db is empty, add data
                            for(int x=0; x<list.size();x++){
                                databaseHandler.addTVProgram(list.get(x));
                            }
                        }

                    }else{
                        Toast.makeText(this, "list size = 0", Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(this, "All Programs are Displayed", Toast.LENGTH_SHORT).show();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private ConnectivityManager connManager;
    private NetworkInfo mWifi;
    private NetworkInfo mMobile;

    // Check Internet Connection
    public boolean checkConnection(Activity activity) {
        connManager = (ConnectivityManager) activity.getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        mMobile = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (mWifi.isConnected() || mMobile.isConnected()) {
            return true;
        }

        return false;
    }
}
