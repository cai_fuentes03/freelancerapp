package com.example.myapplication;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.myapplication.data.TVData;

import java.util.ArrayList;

/**
 * Created by Android 18 on 5/11/2015.
 */
public class DataAdapter extends BaseAdapter{

    private ArrayList<TVData> data;
    private LayoutInflater layoutInflater;

    public DataAdapter(Activity ac, ArrayList<TVData> data){
        this.data = data;
        this.layoutInflater = (LayoutInflater) ac.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Holder holder = null;

        if(view==null){
            holder = new Holder();
            view = layoutInflater.inflate(R.layout.data_adapter, null);

//            holder.name_tv = (TextView) view.findViewById(R.id.adapter_name);
//            holder.starttime_tv = (TextView) view.findViewById(R.id.adapter_start_time);
//            holder.endtime_tv = (TextView) view.findViewById(R.id.adapter_end_time);
//            holder.channel_tv = (TextView) view.findViewById(R.id.adapter_channel);
//            holder.rating_tv = (TextView) view.findViewById(R.id.adapter_rating);


        }else{
            holder = (Holder) view.getTag();
        }

        TextView name_tv = (TextView) view.findViewById(R.id.adapter_name);
        TextView starttime_tv = (TextView) view.findViewById(R.id.adapter_start_time);
        TextView endtime_tv = (TextView) view.findViewById(R.id.adapter_end_time);
        TextView channel_tv = (TextView) view.findViewById(R.id.adapter_channel);
        TextView rating_tv = (TextView) view.findViewById(R.id.adapter_rating);

        name_tv.setText(data.get(i).name);
        starttime_tv.setText(data.get(i).start_time);
        endtime_tv.setText(data.get(i).end_time);
        channel_tv.setText("Channel: " + data.get(i).channel);
        rating_tv.setText("Rating: " + data.get(i).rating);

        return view;
    }

    public class Holder{
        public TextView name_tv;
        public TextView starttime_tv;
        public TextView endtime_tv;
        public TextView channel_tv;
        public TextView rating_tv;
    }
}
