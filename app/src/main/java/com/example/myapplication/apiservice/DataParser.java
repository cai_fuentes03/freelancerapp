package com.example.myapplication.apiservice;

import com.example.myapplication.data.TVData;
import com.example.myapplication.data.TVList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DataParser {

	private JSONObject mRoot = null;
	
	public boolean parse(String xml, DataType type) {
		if (type == DataType.XML) {
			// Develop later if needed
			return false;
		} else {
			try {
				mRoot = new JSONObject(xml);
				return true;
			} catch (JSONException e) {
				e.printStackTrace();
				return false;
			}
		}
	}

	public TVList getData()
	{
        TVList listing = new TVList();
		if(mRoot == null)
		{
            listing.status = -1;
			return listing;
		}
		
		try {
            listing.status = 1;

//            String response = mRoot.getString("response");
//            JSONObject obj1 = new JSONObject(response);
            String data = mRoot.getString("results");
            JSONArray array = new JSONArray(data);
            listing.data_list = new ArrayList<TVData>();


            for (int i=0; i<array.length(); i++){
                TVData tvd = new TVData();
                JSONObject res =  array.getJSONObject(i);

                try {
                    if(res.has("name"))
                        tvd.name = res.getString("name");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if(res.has("start_time"))
                        tvd.start_time = res.getString("start_time");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if(res.has("end_time"))
                        tvd.end_time = res.getString("end_time");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if(res.has("channel"))
                        tvd.channel = res.getString("channel");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if(res.has("rating"))
                        tvd.rating = res.getString("rating");
                } catch (Exception e) {
                    e.printStackTrace();
                }

                listing.data_list.add(tvd);
            }


			return listing;
		} catch (Exception e) {
			e.printStackTrace();
            listing.status = -1;
			return listing;
		}
	}

    public enum DataType {
        XML,
        JSON
    }

}

