package com.example.myapplication.apiservice;

public enum ResultCode {
	Success, 
	Failed, 
	ServerError, 
	NetworkError,
	Cancel,
	Unknown
}