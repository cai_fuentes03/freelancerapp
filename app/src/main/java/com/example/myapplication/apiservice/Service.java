package com.example.myapplication.apiservice;

import android.os.Handler;
import android.os.Message;

import com.example.myapplication.configs.Config;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HttpsURLConnection;

public class Service implements Runnable{

	private HttpURLConnection mConnection;
	private Thread mThread;
	private String mEntityType;
	private static Service mService;
	private static OnServiceListener mServiceListener;
	private ServiceAction mServiceAction;
	private boolean mConnecting;
	private String mUrl;
	private Map<String, String> mParams;
	
	public Service() {
		this(null);
	}
	
	public Service(OnServiceListener serviceListener)
	{
		mServiceAction = ServiceAction.ActionNone;
		mServiceListener = serviceListener;
		mService = this;
		mConnecting = false;
	}
	
	private String getParamsString(Map<String, String> params) throws UnsupportedEncodingException {
		if (params == null)
			return null;

		
		String ret = "";
		for (String key : params.keySet()) {
			String value = params.get(key);
				ret += key + "=" + URLEncoder.encode(value,"UTF-8") + "&";
		}
		return ret;
	}	

	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		try {
			String urlString = mUrl;
			String data = getParamsString(mParams);

			if (data != null)
				if (mEntityType.equals("GET") && !data.equals(""))
					urlString = urlString + "?" + data;
				else
				{
					if(data.endsWith("&"))
						data = data.substring(0, data.length()-1);
				}

			String charset = "UTF-8";
			if(urlString.endsWith("&"))
			{
				urlString = urlString.substring(0, urlString.length()-1);
			}

			URL url = new URL(urlString);

			mConnection = (HttpURLConnection) url.openConnection();
			
			switch (mServiceAction) {
			
			case ActionCallAPI:
				break;
			default:
				break;
			}

            mConnection.setRequestProperty("Accept-Charset", charset);
            mConnection.setRequestProperty("Content-Type",
                        "application/x-www-form-urlencoded;charset=" + charset);

            mConnection.setRequestMethod(mEntityType);
            String userAgent = "Dalvik/1.6.0 (Linux; U;)";

            mConnection.addRequestProperty("User-Agent", userAgent);
            mConnection.setDoInput(true);
                if (mEntityType.equals("POST")) {
                    // post data
                    mConnection.setDoOutput(true);
                    try {
                        OutputStream out = new BufferedOutputStream(
                                mConnection.getOutputStream());
                        out.write(data.getBytes());
                        out.flush();
                        out.close();
                    } catch (IOException ioe) {
                        throw ioe;
                    }
                }

			int httpCode = mConnection.getResponseCode();

			if (httpCode == HttpsURLConnection.HTTP_OK) {

					InputStream in;
					if (mConnection.getHeaderField("Content-Encoding") != null
							&& mConnection.getHeaderField("Content-Encoding")
									.trim().toLowerCase().equals("gzip"))
						in = new GZIPInputStream(mConnection.getInputStream());
					else
						in = new BufferedInputStream(mConnection.getInputStream());

					try{
						String temp = Util.convertStreamToString(in);
	
						temp = temp.replace("&quot;", "\\\"");
						temp = temp.replace("&lt;", "<");
						temp = temp.replace("&gt;", ">");
						temp = temp.replace("&apos;", "'");
						temp = temp.replace("&amp;", "and");
						temp = temp.replace("<pre>", "");
						temp = temp.replace("\"", "\"");
						temp = temp.replace("<sup>", " ");
						temp = temp.replace("</", "");
						temp = temp.replace("sup>", "");
						 
						System.out.println("else URL: " + urlString);
						System.out.println("Service.run() here = " + temp);

						dispatchResult(temp);
					}catch(Exception e){
						e.printStackTrace();
					}
				
			} else if (httpCode == HttpsURLConnection.HTTP_NOT_FOUND) {

				processError(ResultCode.Failed);

			} else if (httpCode == HttpsURLConnection.HTTP_INTERNAL_ERROR) {

				processError(ResultCode.ServerError);

			} else {
				processError(ResultCode.NetworkError);

			}
		} catch (Exception ex) {
			ex.printStackTrace();

			processError(ResultCode.NetworkError);
		} finally {
			cleanUp();
		}
	}

	private void dispatchResult(String result)
	{
		if (mServiceListener == null || mServiceAction == ServiceAction.ActionNone
				|| !mConnecting)
			return;
		ServiceAction act = mServiceAction;
		Object resObj = null;
		ServiceResponse response = null;
		DataParser parser = new DataParser();
		
		boolean isSuccess;

		isSuccess = parser.parse(result, DataParser.DataType.JSON);
		
		if (isSuccess) {
			switch (act) {
			case ActionCallAPI:
				resObj = parser.getData();
				break;
			default:
				break;
			}
		}
		else {
		}
		
		if (resObj == null){
            System.out.print("service resobj null");
			response = new ServiceResponse(act, null, ResultCode.Failed);
		}
		else
			response = new ServiceResponse(act, resObj);
		
		stop();

		Message msg = handler.obtainMessage(0, response);
		handler.sendMessage(msg);
	}
	
	public void stop() {
		cleanUp();
	}
	
	private void cleanUp() {

		mServiceAction = ServiceAction.ActionNone;
		if (mConnection != null) {
			try {
				mConnection.disconnect();
			} catch (Exception ex) {
				// do nothing
			}
			mConnection = null;
		}
		mConnecting = false;
		
	}
	
	private void processError(ResultCode errorCode) {
		Message msg = handler.obtainMessage(0, new ServiceResponse(mServiceAction,
				null, errorCode));
		handler.sendMessage(msg);
	}
	
	// Handler
	protected static final Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (mServiceListener != null) {
				mServiceListener.onCompleted(mService, (ServiceResponse) msg.obj);
			}
		}
		};

	public void call_api(String start){

		mServiceAction = ServiceAction.ActionCallAPI;
		String urlSend = Config.API;
		Map<String, String> params = new HashMap<String, String>();
		params.put("start", start);
		request(urlSend, params, "GET");
	}

    private boolean request(String url, Map<String, String> params,
                            String entityType)
    {
        if (mConnecting)
            return false;
        mConnecting = true;
        mUrl = url;
        mParams = params;
        mEntityType = entityType;
        mThread = new Thread(this);
        mThread.start();
        return true;
    }
}
