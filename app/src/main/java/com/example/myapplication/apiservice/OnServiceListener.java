package com.example.myapplication.apiservice;


public interface OnServiceListener {
	/*
	 * Called when a request has been finished without canceled.
	 */
	public void onCompleted(Service service, ServiceResponse result);

}
